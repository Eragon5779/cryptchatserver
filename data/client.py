from datetime import datetime
from sys import exc_info

from utils.parser import read as pread
from utils.parser import write as pwrite


class Client:
    def __init__(self, sid, socket):
        self.sid = sid
        self.socket = socket
        self.run = True
        self.token = None

    def set_token(self, token):
        self.token = token

    def send_data(self, data, closing=False):
        d = pwrite(data)
        try:
            self.socket.sendall(d)
        except Exception as e:
            print(
                f"[Warning][{datetime.now()}] Client {self.sid} raised {type(e).__name__} while sending"
            )
            if type(e).__name__ == BrokenPipeError and not closing:
                self.close()

    def close(self):
        self.send_data({"connection": "closed"}, True)
        self.socket.close()
        self.run = False
