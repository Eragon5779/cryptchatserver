from utils.db import db
import config

mdb = db(**config.server)
users = mdb.client.cryptchat.user
chats = mdb.client.cryptchat.chat
msgs = mdb.client.cryptchat.message
