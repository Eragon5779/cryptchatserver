import handlers.auth, handlers.msg, handlers.chat
from utils.parser import read as pread, write as pwrite
import time, sys
from json import JSONDecodeError
from data.vars import users, chats, msgs
from utils.notifications import send_notification


class Client:
    def __init__(self, sid, socket):
        self.sid = sid
        self.socket = socket

    run = True
    token = None

    def set_token(self, token):
        self.token = token

    def send_data(self, data):
        d = pwrite(data)
        self.socket.send(d)

    def close(self):
        self.send_data({"connection": "closed"})
        self.socket.close()
        self.run = False


def run_command(path, command, data):
    module = getattr(globals()["handlers"], path)
    handler = getattr(module, command)
    return handler(**data)


def recieve_chunk(c):
    return c.socket.recv(1024)


def handle_client_connection(c, clients, threads):
    print(f"[Debug] Handling client {c.sid}")
    running = True
    while running:
        request = b""
        results = b""
        while True:
            try:
                request += recieve_chunk(c)
            except ConnectionResetError:
                running = False
                break
            except BrokenPipeError:
                running = False
                break
            except TimeoutError:
                running = False
                break
            except Exception as e:  # Catch-all to try to keep threads down
                print(
                    f"Unhandled exception occurred with client!\n    Error type: {type(e).__name__}\n    Error args: {e.args}"
                )
                running = False
                break
            try:
                data = pread(request)
                break
            except JSONDecodeError:
                pass
        if not running:
            break
        print(f"[Debug] Received data: {data}")
        if "connection" in data.keys():
            if data["connection"] == "close":
                running = False
                break
            elif data["connection"] == "sid":
                results = {"type": "sid", "data": {"sid": c.sid}}
        if "path" in data.keys():
            results = run_command(data["path"], data["cmd"], data["data"])
        if results["type"] == "auth" and not "error" in results["data"]:
            if "token" in results["data"]:
                clients[clients.index(c)].set_token(results["data"]["token"])
                c.set_token(results["data"]["token"])
        if "notifs" in data.keys():
            notifs = data["notifs"]
            del data["notifs"]
            for notif in notifs:
                send_notification(**notif)
        if running:
            c.send_data(results)
    try:
        c.close()
    except:
        print(f"[Warning] Client {str(c.sid)} closed ungracefully")
    clients.remove(c)
    return
