import json
import signal
import sys
import threading
import time
from datetime import datetime
from multiprocessing import Manager, Process
from socket import AF_INET, SOCK_STREAM, socket

from bson.objectid import ObjectId

import handlers.auth
import handlers.chat
import handlers.msg
from data.client import Client
from data.vars import chats, msgs, users
from utils.parser import read as pread
from utils.parser import write as pwrite
from utils.sid import gen_sid
from utils.tokens import get_token

running = True
bind_ip = "0.0.0.0"
bind_port = 55132

server = socket(AF_INET, SOCK_STREAM)
server.settimeout(None)
server.bind((bind_ip, bind_port))

mgr = Manager()

clients = mgr.list([])
threads = []

##region notifications


def send_notification(type, data):
    if type == "new_msg":
        new_msg(data)
    if type == "new_chat":
        new_chat(data)
    if type == "del_msg":
        deleted_msg(data)
    if type == "edit_msg":
        edited_msg(data)


def edited_msg(data):
    _id = data["_id"]
    mid = data["mid"]
    cid = data["cid"]
    if type(_id) == str:
        _id = ObjectId(_id)
    if type(cid) == str:
        cid = ObjectId(cid)
    token = get_token(_id)
    if not token:
        return  # Should never get here
    c = [x for x in clients if x.token == token]
    if not c:
        queue = chats.find_one({"_id": cid})["queue"]
        queue[str(_id)]["edited"].append(mid)
        chats.update_one({"_id": cid}, {"$set": {"queue": queue}})
        return
    if type(mid) == str:
        mid = ObjectId(mid)
    m = msgs.find_one({"_id": mid})
    data = {
        "type": "edit_msg",
        "data": {
            "_id": str(mid),
            "edited_at": m["edited_at"],
            "signature": m["signature"],
            "message": m["message"],
        },
    }
    for x in c:
        x.send_data(data)


def deleted_msg(data):
    _id = data["_id"]
    mid = data["mid"]
    cid = data["cid"]
    if type(_id) == str:
        _id = ObjectId(_id)
    if type(cid) == str:
        cid = ObjectId(cid)
    token = get_token(_id)
    if not token:
        return  # Should never get here
    c = [x for x in clients if x.token == token]
    data = {"type": "del_msg", "data": {"_id": str(mid)}}
    if not c:
        queue = chats.find_one({"_id": cid})["queue"]
        queue[str(_id)]["deleted"].append(mid)
        chats.update_one({"_id": cid}, {"$set": {"queue": queue}})
        return
    for x in c:
        x.send_data(data)


def new_chat(data):
    _id = data["_id"]
    chat = data["chat"]
    if type(_id) == str:
        _id = ObjectId(_id)
    chat["_id"] = str(chat["_id"])
    token = get_token(_id)
    if not token:
        return  # Should never get here
    c = [x for x in clients if x.token == token]
    if not c:
        return
    tmp = [x for x in chat["members"]]
    for member in tmp:
        chat["members"].remove(member)
        chat["members"].append(
            str(users.find_one({"_id": ObjectId(member)})["username"])
        )
    data = {"type": "new_chat", "data": chat}
    for x in c:
        x.send_data(data)


def new_msg(data):
    _id = data["_id"]
    msg = data["msg"]
    if type(_id) == str:
        _id = ObjectId(_id)
    token = get_token(_id)
    if not token:
        return  # Should never get here
    c = [x for x in clients if x.token == token]
    if not c:
        return
    msg["sender"] = users.find_one({"_id": ObjectId(msg["sender"])})["username"]
    msg["_id"] = str(msg["_id"])
    msg["chat"] = str(msg["chat"])
    msg["key"] = msg["key"][str(_id)]
    msg["key_encrypted"] = msg.pop("key")
    data = {"type": "new_msg", "data": msg}
    for x in c:
        x.send_data(data)


##endregion

##region manager


def run_command(path, command, data):
    module = getattr(globals()["handlers"], path)
    handler = getattr(module, command)
    return handler(**data)


def handle_client_connection(c):
    running = True
    while running:
        request = b""
        results = b""
        if not c.run:
            break
        try:
            rcv = c.socket.recv(65536)
            if rcv:
                request += rcv
                # print(f"Received: {rcv}")
            else:
                break
        except Exception as e:
            print(
                f"[Warning][{datetime.now()}] Client {c.sid} raised error {type(e).__name__}"
            )
            running = False
            break
        if not running:
            break
        data = pread(request)
        # print(f"Data: {data}")
        if "connection" in data.keys():
            if data["connection"] == "close":
                running = False
                c.send_data({"type": "disconnect", "data": {"status": "success"}})
                break
            elif data["connection"] == "sid":
                # print(f"Got SID request")
                results = {"type": "sid", "data": {"sid": c.sid}}
        if "path" in data:
            results = run_command(data["path"], data["cmd"], data["data"])
        if results["type"] == "auth" and not "error" in results["data"]:
            if "token" in results["data"]:
                c.set_token(results["data"]["token"])
                for i, x in enumerate(clients):
                    if x.sid == c.sid:
                        clients[i] = c
                        break
        if "notifs" in results:
            notifs = results["notifs"]
            results.pop("notifs", None)
            for notif in notifs:
                send_notification(**notif)
        if running:
            c.send_data(results)
    try:
        c.close()
    except:
        print(f"[Warning][{datetime.now()}] Client {str(c.sid)} closed ungracefully")
    return


##endregion

##region main


print(f"[Info][{datetime.now()}] Listening on {bind_ip}:{bind_port}")


def close_server(sig, frame):
    print(f"\n[Warning][{datetime.now()}] Shutting down server...")
    running = False
    sys.exit(0)


def accept_incoming():
    while running:
        server.listen(100)  # max backlog of connections
        old_threads = [x for x in threads if not x.is_alive()]
        old_clients = [x for x in clients if not x.run]
        for x in old_threads:
            x.terminate()
            threads.remove(x)
        for x in old_clients:
            clients.remove(x)
        client_sock, address = server.accept()
        c = Client(str(gen_sid()), client_sock)
        clients.append(c)
        thread = Process(target=handle_client_connection, args=(c,))
        threads.append(thread)
        thread.start()
    print("[Info] Closing threads...")
    for t in threads:
        t.join(10)
    server.close()


signal.signal(signal.SIGINT, close_server)


def start():
    # server.listen(100)  # max backlog of connections
    # accept_thread = Process(target=accept_incoming)
    # accept_thread.start()
    # accept_thread.join()
    # server.close()
    accept_incoming()


##endregion
