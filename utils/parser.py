import json


def read(b):
    return json.loads(b)


def write(j):
    if type(j) == str:
        return j.encode("UTF-8")
    return json.dumps(j).encode("UTF-8")
