from bson.objectid import ObjectId
import hashlib, time
from base64 import b64encode as b64
import ulid


def generate_token():
    u = ulid.new()
    return u.str


def generate_id(data):
    return ObjectId(
        hashlib.sha3_512(
            str(data).encode("UTF-8") + str(time.time()).encode("UTF-8")
        ).hexdigest()[:24]
    )


def generate_chat_id(data):
    return ObjectId(hashlib.sha3_512(data.encode("UTF-8")).hexdigest()[:24])
