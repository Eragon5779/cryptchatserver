from data.vars import users


def find_token(token):
    """Searches the user table for specified token"""
    user = users.find_one({"token": token})
    if not user:
        return False
    return True


def get_token_owner(token):
    user = users.find_one({"token": token})
    if not user:
        return None
    return {"_id": user["_id"], "username": user["username"]}


def check_ownership(token, username):
    user = users.find_one({"token": token})
    if user["username"] == username:
        return True
    return False


def get_token(_id):
    user = users.find_one({"_id": _id})
    if not user:
        return None
    return user["token"]
