from utils.tokens import get_token
from bson.objectid import ObjectId
from data.vars import users, chats, msgs


def send_notification(type, data):
    if type == "new_msg":
        new_msg(data)
    if type == "new_chat":
        new_chat(data)
    if type == "del_msg":
        deleted_msg(data)
    if type == "edit_msg":
        edited_msg(data)


def deleted_msg(data):
    _id = data["_id"]
    mid = data["mid"]
    cid = data["cid"]
    if type(_id) == str:
        _id = ObjectId(_id)
    if type(cid) == str:
        cid = ObjectId(cid)
    token = get_token(_id)
    if not token:
        return  # Should never get here
    c = next((x for x in clients if x.token == token), None)
    if not c:
        queue = chats.find_one({"_id": cid})["queue"]
        queue[str(_id)]["edited"].append(mid)
        chats.update_one({"_id": cid}, {"$set": {"queue": queue}})
        return


def deleted_msg(data):
    _id = data["_id"]
    mid = data["mid"]
    cid = data["cid"]
    if type(_id) == str:
        _id = ObjectId(_id)
    if type(cid) == str:
        cid = ObjectId(cid)
    token = get_token(_id)
    if not token:
        return  # Should never get here
    c = next((x for x in clients if x.token == token), None)
    if not c:
        queue = chats.find_one({"_id": cid})["queue"]
        queue[str(_id)]["deleted"].append(mid)
        chats.update_one({"_id": cid}, {"$set": {"queue": queue}})
        return


def new_chat(data):
    _id = data["_id"]
    chat = data["chat"]
    if type(_id) == str:
        _id = ObjectId(_id)
    chat["_id"] = str(chat["_id"])
    token = get_token(_id)
    if not token:
        return  # Should never get here
    c = next((x for x in clients if x.token == token), None)
    if not c:
        return
    tmp = [x for x in chat["members"]]
    for member in tmp:
        chat["members"].remove(member)
        chat["members"].append(
            str(users.find_one({"_id": ObjectId(member)})["username"])
        )
    data = {"type": "new_chat", "data": chat}
    c.send_data(data)


def new_msg(data):
    _id = data["_id"]
    msg = data["msg"]
    if type(_id) == str:
        _id = ObjectId(_id)
    token = get_token(_id)
    if not token:
        return  # Should never get here
    c = next((x for x in clients if x.token == token), None)
    if not c:
        return
    msg["sender"] = users.find_one({"_id": ObjectId(msg["sender"])})["username"]
    msg["_id"] = str(msg["_id"])
    msg["chat"] = str(msg["chat"])
    msg["key"] = msg["key"][str(_id)]
    data = {"type": "new_msg", "data": msg}
    c.send_data(data)
