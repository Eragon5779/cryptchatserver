from pymongo import MongoClient


class db:
    def __init__(self, user, pwd, mongo, col):
        self.client = MongoClient(f"mongodb://{user}:{pwd}@{mongo}/{col}")
