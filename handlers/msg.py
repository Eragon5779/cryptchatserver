import time

from bson.objectid import ObjectId

from data.vars import chats, msgs, users
from utils import notifications, tokens
from utils.generators import generate_chat_id, generate_id


def updatemsg(token, mid, signature, s_key):
    if not token or not tokens.find_token(token):
        return {"type": "msg", "data": {"error": "invalid token"}}
    if (
        not sender
        or not recipient
        or not s_key
        or not r_key
        or not signature
        or not token
    ):
        return {"type": "msg", "data": {"error": "invalid token"}}

    user = tokens.get_token_owner(token)
    if not user:
        return {"type": "msg", "data": {"error": "invalid token"}}

    m = msgs.find_one({"_id": ObjectId(mid)})
    if not m:
        return {"type": "msg", "data": {"error": "invalid message id"}}
    key = m["key"]
    key[str(user["_id"])] = s_key
    if m["sender"] != str(user["_id"]):
        msgs.update_one({{"_id": ObjectId(mid)}, {"$set": {"key": key}}})
        return {"type": "msg", "data": {"status": "success"}}
    else:
        msgs.update_one(
            {{"_id": ObjectId(mid)}, {"$set": {"signature": signature, "key": key}}}
        )
        rid = [
            x
            for x in chats.find({"_id": ObjectId(m["chat"])})["members"]
            if not x == str(user["_id"])
        ][0]
        # notifications.send_notification("edit_msg", {'_id':rid, 'mid':str(m['_id']), 'cid':m['chat']})
        return {
            "type": "msg",
            "data": {"status": "success"},
            "notifs": [
                {
                    "type": "edit_msg",
                    "data": {"_id": rid, "mid": str(m["_id"]), "cid": m["chat"]},
                }
            ],
        }
    return {"type": "msg", "data": {"error": "invalid message id"}}


def sendmessage(sender, recipient, message, s_key, r_key, signature, token, file = None):
    if not token or not tokens.find_token(token):
        return {"type": "msg", "data": {"error": "invalid token"}}
    if (
        not sender
        or not recipient
        or not s_key
        or not r_key
        or not signature
        or not token
    ):
        return {"type": "msg", "data": {"error": "invalid token"}}
    if not tokens.check_ownership(token, sender):
        return {"type": "msg", "data": {"error": "invalid token"}}

    timestamp = time.time()
    _id = generate_id(message)
    sid = str(users.find_one({"username": sender})["_id"])
    rid = str(users.find_one({"username": recipient})["_id"])
    if not rid:
        return {"type": "msg", "data": {"error": "invalid recipient"}}
    clist = [sid, rid]
    clist.sort()
    cdata = "".join(clist)
    chat = generate_chat_id(cdata)

    m = {
        "_id": _id,
        "chat": chat,
        "message": message,
        "key": {str(sid): s_key, str(rid): r_key},
        "signature": signature,
        "timestamp": timestamp,
        "sender": sid,
        "file": file
    }
    nc = False
    c = None
    queue = {}
    for us in clist:
        queue[us] = {"deleted": [], "edited": [], "resync": []}
    if not chats.find_one({"_id": chat}):
        c = {"_id": chat, "members": clist, "msg_count": 1, "queue": queue}
        chats.insert_one(c)
        # notifications.send_notification("new_chat", {"_id":rid, "chat":c})
        nc = True
    else:
        c = chats.find_one({"_id": chat})
        c["msg_count"] += 1
        chats.update_one({"_id": chat}, {"$set": {"msg_count": c["msg_count"]}})

    msgs.insert_one(m)
    # notifications.send_notification("new_msg", {"_id":rid, "msg":m})
    if nc:
        return {
            "type": "msg",
            "data": {"_id": str(_id), "chat": str(chat), "timestamp": timestamp},
            "notifs": [
                {"type": "new_chat", "data": {"_id": rid, "chat": c}},
                {"type": "new_msg", "data": {"_id": rid, "msg": m}},
            ],
        }
    return {
        "type": "msg",
        "data": {"_id": str(_id), "chat": str(chat), "timestamp": timestamp},
        "notifs": [{"type": "new_msg", "data": {"_id": rid, "msg": m}}],
    }


def getedit(token, mid):
    if not token or not tokens.find_token(token):
        return {"type": "msg", "data": {"error": "invalid token"}}
    if not mid:
        return {"type": "msg", "data": {"error": "invalid token"}}
    user = tokens.get_token_owner(token)
    if not user:
        return {"type": "msg", "data": {"error": "invalid token"}}

    m = msgs.find_one({"_id": ObjectId(mid)})
    if not m:
        return {"type": "msg", "data": {"error": "invalid message id"}}

    return {"type": "msg", "data": {"_id": m["edited_at"]}}


def deletemsg(token, mid, username):
    # token sanity
    if not token or not tokens.find_token(token):
        return {"type": "msg", "data": {"error": "invalid token"}}
    if not mid:
        return {"type": "msg", "data": {"error": "invalid token"}}
    if not tokens.check_ownership(token, username):
        return {"type": "msg", "data": {"error": "invalid token"}}

    user = tokens.get_token_owner(token)
    if not user:
        return {"type": "msg", "data": {"error": "invalid token"}}

    # check message ownership
    m = msgs.find_one({"_id": ObjectId(mid)})
    if not m:
        return {"type": "msg", "data": {"error": "invalid message id"}}
    if m["sender"] != str(user["_id"]):
        return {"type": "msg", "data": {"error": "invalid message id"}}

    # delete message
    msgs.delete_one({"_id": ObjectId(mid)})
    rid = [
        x
        for x in chats.find_one({"_id": ObjectId(m["chat"])})["members"]
        if not x == str(user["_id"])
    ][0]
    # notifications.send_notification("del_msg", {'_id':rid, 'mid':str(m['_id']), 'cid':m['chat']})
    return {
        "type": "msg",
        "data": {"status": "success"},
        "notifs": [
            {
                "type": "del_msg",
                "data": {"_id": rid, "mid": str(m["_id"]), "cid": m["chat"]},
            }
        ],
    }


def editmsg(token, mid, username, message, signature):
    # token sanity
    if not token or not tokens.find_token(token):
        return {"type": "msg", "data": {"error": "invalid token"}}
    if not mid:
        return {"type": "msg", "data": {"error": "invalid token"}}
    if not tokens.check_ownership(token, username):
        return {"type": "msg", "data": {"error": "invalid token"}}

    user = tokens.get_token_owner(token)
    if not user:
        return {"type": "msg", "data": {"error": "invalid token"}}

    # check message ownership
    m = msgs.find_one({"_id": ObjectId(mid)})
    if not m:
        return {"type": "msg", "data": {"error": "invalid message id"}}
    if m["sender"] != str(user["_id"]):
        return {"type": "msg", "data": {"error": "invalid message id"}}

    # update message
    timestamp = time.time()
    msgs.update_one(
        {"_id": ObjectId(mid)},
        {
            "$set": {
                "edited": True,
                "edited_at": timestamp,
                "message": message,
                "signature": signature,
            }
        },
    )
    m = msgs.find_one({"_id": ObjectId(mid)})
    rid = [
        x
        for x in chats.find_one({"_id": ObjectId(m["chat"])})["members"]
        if not x == str(user["_id"])
    ][0]
    # notifications.send_notification("upd_msg", {'_id':rid, 'msg':m})
    return {
        "type": "msg",
        "data": {"edited": True, "edited_at": timestamp, "_id": mid},
        "notifs": [
            {
                "type": "edit_msg",
                "data": {"_id": rid, "mid": str(mid), "cid": str(m["chat"])},
            }
        ],
    }
