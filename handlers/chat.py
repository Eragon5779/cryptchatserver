from utils import tokens
from bson.objectid import ObjectId
import hashlib, time
from data.vars import users, chats, msgs


def getqueue(token, _id):
    if not token or not tokens.find_token(token):
        return {"type": "chat", "data": {"error": ["invalid token"]}}
    if not _id:
        return {"type": "chat", "data": {"error": ["invalid token"]}}
    user = tokens.get_token_owner(token)
    if not user:
        return {"type": "chat", "data": {"error": ["invalid token"]}}

    chat = chats.find_one({"_id": ObjectId(_id)})
    if not chat:
        return {"type": "chat", "data": {"error": ["invalid token"]}}

    queue = chat["queue"][str(user["_id"])]
    other_queue = chat["queue"]
    other_queue[str(user["_id"])] = {"deleted": [], "edited": [], "resync": []}
    chats.update_one({"_id": ObjectId(_id)}, {"$set": {"queue": other_queue}})
    return {"type": "queue", "data": queue}


def resync(token, cid, uid):
    if not token or not tokens.find_token(token):
        return {"type": "chat", "data": {"error": "invalid token"}}
    if not cid:
        return {"type": "chat", "data": {"error": "invalid token"}}
    user = tokens.get_token_owner(token)
    if not user:
        return {"type": "chat", "data": {"error": "invalid token"}}

    chat = chats.find_one({"_id": ObjectId(cid)})
    if not chat:
        return {"type": "chat", "data": {"error": "invalid token"}}

    msglist = {"type": "chat", "data": {}}

    ms = msgs.find({"chat": ObjectId(cid)})

    for m in ms:
        if m["sender"] == uid:
            msglist["data"]["_id"] = m["signature"]

    return msglist


def getall(token):
    if not token or not tokens.find_token(token):
        return {"type": "chat", "data": {"error": "invalid token"}}
    user = tokens.get_token_owner(token)
    if not user:
        return {"type": "chat", "data": {"error": "invalid token"}}

    chat_list = {"type": "clist", "data": []}
    for chat in chats.find({"members": str(user["_id"])}):
        chat["_id"] = str(chat["_id"])
        tmp = [x for x in chat["members"]]
        for member in tmp:
            chat["members"].remove(member)
            chat["members"].append(
                str(users.find_one({"_id": ObjectId(member)})["username"])
            )
        chat_list["data"].append(
            {
                "_id": chat["_id"],
                "members": chat["members"],
                "msg_count": chat["msg_count"],
            }
        )

    return chat_list


def getchat(_id, token):
    if not token or not tokens.find_token(token):
        return {"type": "chat", "data": {"error": "invalid token"}}
    if not _id:
        return {"type": "chat", "data": {"error": "invalid token"}}
    user = tokens.get_token_owner(token)
    if not user:
        return {"type": "chat", "data": {"error": "invalid token"}}

    chat = chats.find_one({"_id": ObjectId(_id)})
    if not chat:
        return {"type": "chat", "data": {"error": "invalid token"}}
        tmp = [x for x in chat["members"]]
        for member in tmp:
            chat["members"].remove(member)
            chat["members"].append(
                str(users.find_one({"_id": ObjectId(member)})["username"])
            )
    return {
        "type": "chat",
        "data": {
            "_id": chat["_id"],
            "members": chat["members"],
            "msg_count": chat["msg_count"],
        },
    }


def getnew(chat_id, oldest, token):
    if not token or not tokens.find_token(token):
        return {"type": "chat", "data": {"error": "invalid token"}}
    if not chat_id:
        return {"type": "chat", "data": {"error": "chat not specified"}}
    user = tokens.get_token_owner(token)
    if not user:
        return {"error": "invalid token"}
    if not oldest:
        oldest = 0.0
    else:
        try:
            oldest = float(oldest)
        except:
            oldest = -1

    chat = chats.find_one({"_id": ObjectId(chat_id)})
    if not str(user["_id"]) in chat["members"]:
        return {"type": "chat", "data": {"error": "invalid chat id"}}
    if not chat:
        return {"type": "chat", "data": {"error": "invalid chat id"}}

    msglist = {"type": "cmlist", "data": []}
    for msg in msgs.find({"timestamp": {"$gt": oldest}, "chat": ObjectId(chat_id)}):
        msg["sender"] = users.find_one({"_id": ObjectId(msg["sender"])})["username"]
        msg["_id"] = str(msg["_id"])
        msg["chat"] = str(msg["chat"])
        msg["key"] = msg["key"][str(user["_id"])]
        msglist["data"].append(msg)

    return msglist
