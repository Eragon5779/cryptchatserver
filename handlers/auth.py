from utils.generators import generate_id, generate_token
from data.vars import users, chats, msgs


def getsalt(username):
    u = users.find_one({"username": username})
    if not u:
        return {"type": "auth", "data": {"salt": "lasdfj908"}}
    return {"type": "auth", "data": {"salt": u["salt"]}}


def login(username, hash):
    u = users.find_one({"username": username})
    if not u or hash != u["hash"]:
        return {"type": "auth", "data": {"error": "login failed"}}
    return {
        "type": "auth",
        "data": {
            "_id": str(u["_id"]),
            "username": username,
            "public": u["public"],
            "token": u["token"],
        },
    }


def getuser(username):
    u = users.find_one({"username": username})
    if not u:
        return {"type": "auth", "data": {"error": "user retrieval failed"}}
    return {"type": "auth", "data": {"username": username, "public": u["public"]}}


def register(username, hash, public, salt):
    if not username or not hash or not public or not salt:
        return {"type": "auth", "data": {"error": "missing registration data"}}
    _id = generate_id(username)
    token = generate_token()
    if users.find_one({"username": username}):
        return {"type": "auth", "data": {"error": "registration failed"}}
    u = {
        "_id": _id,
        "username": username,
        "hash": hash,
        "salt": salt,
        "public": public,
        "token": token,
    }
    users.insert_one(u)
    return {
        "type": "auth",
        "data": {
            "_id": str(_id),
            "username": username,
            "salt": salt,
            "public": public,
            "token": token,
        },
    }
