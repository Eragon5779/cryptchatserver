from utils import tokens, generators
from data.vars import users, chats, msgs

def changepass(token, old_hash, new_hash, public, salt) :
    # verify token
    if not token or not tokens.find_token(token) :
        return {'type':'acct','data':{'error':'invalid token'}}
    user = tokens.get_token_owner(token)
    if not user :
        return {'type':'acct','data':{'error':'invalid token'}}

    # verify user based on old hash
    user = users.find_one({'hash':old_hash, 'token':token, 'username':user['username'], '_id':user['_id']})
    if not user :
        return {'type':'acct','data':{'error':'auth failed'}}

    # update the account
    new_token = generators.generate_token()
    try :
        users.update_one(
            {'_id':user['_id']},
            {'$set': {
                    'token':new_token,
                    'hash':new_hash,
                    'public'public,
                    'salt':salt
                }
            }
        )
        return {
            'type':'acct',
            'data': {
                'token':new_token
            }
        }
    except :
        return {'type':'acct','data':{'error':'update failed'}}
