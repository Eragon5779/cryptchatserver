# CryptChat API

## About

CryptChat is a project designed to bring end-to-end encrypted messaging to the masses

### Document Formatting

For all API usage documentations, the first set of JSON is the success return, and the second set is the failure return, with a few exceptions where there is only one return

### Standards

#### Messages

For messages, we use synchronous encryption (AES256) with an asynchronously encrypted (RSA4096) randomized key that is sent alongside the message. **Messages are encrypted client-side, and never reach the server plaintext**

#### Private Keys

As this is the API for the messaging system, **private keys are never stored or generated on the server itself. All keys are generated and stored client-side**. The official CryptChat client (found [here](https://gitlab.com/Eragon5779/CryptChat) for Windows clients) uses AES256 for private key encryption.

## Usage

### Auth

root: `/auth`

* `login`
  * path: `/login`
  * parameters:
    * `username`
    * `hash`
  * returns:
```json
{
  "login":"success",
  "userdata":
  {
    "username":"<username>",
    "public":"<public key>",
    "token":"<unique user token>"
  }
}
```
```json
{
  "login":"failure"
}
```

* `getsalt`
  * path: `/getsalt`
  * parameters:
    * `username`
  * returns:
```json
{
  "salt":"<user salt>"
}
```
**The above can return random data. This is done for authentication security.**

* `getuser`
  * path: `/getuser`
  * parameters:
    * `username`
  * returns:
```json
{
  "username":"<username>",
  "public": "<public key>"
}
```
```json
{}
```

* `register`
  * path: `/register`
  * parameters:
    * `username`
    * `hash` (prepended by the number of PBKDF2 iterations! i.e. `10000:<hash>`)
    * `public`
    * `salt`
  * returns:
```json
{
  "registration":"success",
  "data":
  {
    "username":"<username>",
    "salt":"<salt>",
    "public":"<public key>",
    "token":"<user token>"
  }
}
```
```json
{
  "registration":"failure"
}
```
